<?php

namespace app\controllers;

use app\models\Lugardestino;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LugardestinoController implements the CRUD actions for Lugardestino model.
 */
class LugardestinoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                // acciones del controlador que voy a gestionar 
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'], // @ indica que el usuario está logeado  
                    ],
                    [
                        'actions' => [''], // el usuario no logeado no puede hacer nada
                        'allow' => true,
                        'roles' => ['?'], // ? indica que el usuario no está logeado
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Lugardestino models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Lugardestino::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idLugar' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lugardestino model.
     * @param int $idLugar Id Lugar
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idLugar)
    {
        return $this->render('view', [
            'model' => $this->findModel($idLugar),
        ]);
    }

    /**
     * Creates a new Lugardestino model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Lugardestino();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idLugar' => $model->idLugar]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Lugardestino model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idLugar Id Lugar
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idLugar)
    {
        $model = $this->findModel($idLugar);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idLugar' => $model->idLugar]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Lugardestino model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idLugar Id Lugar
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idLugar)
    {
        $this->findModel($idLugar)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lugardestino model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idLugar Id Lugar
     * @return Lugardestino the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idLugar)
    {
        if (($model = Lugardestino::findOne(['idLugar' => $idLugar])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
