<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lugardestino".
 *
 * @property int $idLugar
 * @property string|null $foto
 * @property string|null $lugar
 * @property string|null $pais
 * @property string|null $fechaIncicio
 * @property string|null $fechaFinal
 * @property string|null $lugaresAVisitar
 * @property string|null $gastronomia
 * @property string|null $historia
 * @property int|null $continente
 *
 * @property Continente $continente0
 */
class Lugardestino extends \yii\db\ActiveRecord
{
    public $archivo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lugardestino';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idLugar'], 'required'],
            [['idLugar', 'continente'], 'integer'],
            [['fechaIncicio', 'fechaFinal'], 'date', 'format' => 'yyyy-MM-dd'],
            [['lugaresAVisitar', 'gastronomia', 'historia'], 'string'],
            [['foto'], 'string', 'max' => 255],
            [['archivo'], 'file',  'skipOnEmpty' => true, 'mimeTypes' => 'image/*'],
            [['lugar', 'pais'], 'string', 'max' => 50],
            [['idLugar'], 'unique'],
            [['continente'], 'exist', 'skipOnError' => true, 'targetClass' => Continente::class, 'targetAttribute' => ['continente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idLugar' => 'Id Lugar',
            'foto' => 'Foto',
            'lugar' => 'Lugar',
            'pais' => 'País',
            'fechaIncicio' => 'Fecha de inicio',
            'fechaFinal' => 'Fecha de final',
            'lugaresAVisitar' => 'Lugares a visitar',
            'gastronomia' => 'Gastronomía',
            'historia' => 'Historia',
            'continente' => 'Continente',
        ];
    }

    /**
     * Gets query for [[Continente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContinente0()
    {
        return $this->hasOne(Continente::class, ['id' => 'continente']);
    }

    public function getContinentes()
    {
        $continentes = Continente::find()->all();
        return ArrayHelper::map($continentes, 'id', 'continente');
    }

    public function beforeValidate(): bool
    {
        // Comprobamos si has seleccionado una foto 
        if (isset($this->archivo)) {
            $this->archivo = UploadedFile::getInstance($this, "archivo");
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }

    public function afterValidate(): bool
    {
        // Volvemos a comprobar si se ha subido una foto
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->idLugar . '_' . $this->archivo->name; // Al campo foto le guardamos el nombre de la foto
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }


    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/' . $this->idLugar . '_' . $this->archivo->name, false);  // false, si la foto existe, no sube la nueva para no destruir la que está
        return true;
    }
    // Disparador que se ejecuta después de eliminar un registro

    public function afterDelete()
    {
        // Elimino la imagen de la noticia de web/imgs
        if ($this->foto != null && file_exists(Yii::getAlias("@webroot") . '/imgs/' . $this->foto)) {
            unlink('imgs/' . $this->foto);
        }
        return true;
    }
    /**
     * afterSave 
     * Este método se ejecuta después de guardar el registro en la BBDD
     * 
     * @param  mixed $insert este argumento es true si se está insertando un registro y false si es una actualización
     * @param  array $atributosAnteriores Array con todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // Si estoy actualizando datos
        if (!$insert) {
            // Si la noticia tenia ya una foto y hemos adjuntado una nueva, elimina la vieja
            if (isset($this->archivo) && isset($atributosAnteriores['foto'])) {
                unlink('imgs/' . $atributosAnteriores['foto']);
            }
        }
    }

    public function fechaEspana($fecha)
    {
        if (isset($fecha)) {
            return Yii::$app->formatter->asDate($fecha, 'dd-MM-yyyy');
        }
    }
}
