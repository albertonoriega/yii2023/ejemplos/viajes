<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "continente".
 *
 * @property int $id
 * @property string|null $continente
 *
 * @property Lugardestino[] $lugardestinos
 */
class Continente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'continente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['continente'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'continente' => 'Continente',
        ];
    }

    /**
     * Gets query for [[Lugardestinos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLugardestinos()
    {
        return $this->hasMany(Lugardestino::class, ['continente' => 'id']);
    }
}
