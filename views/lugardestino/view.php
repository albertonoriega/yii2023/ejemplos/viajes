<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Lugardestino $model */

$this->title = $model->idLugar;
$this->params['breadcrumbs'][] = ['label' => 'Lugardestinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lugardestino-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idLugar' => $model->idLugar], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idLugar' => $model->idLugar], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Quiere eliminar éste destino?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLugar',
            //'foto',
            [
                'label' => 'Foto',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->foto)) {
                        return Html::img("@web/imgs/{$dato->foto}", ["width" => 300, "height" => 300]);
                    } else {
                        return Html::img("@web/imgs/notfound.jpg", ["width" => 300, "height" => 300]);
                    }
                }
            ],
            'lugar',
            'pais',
            //'fechaIncicio',
            [
                'attribute' => 'fechaIncicio',
                'value' => function ($model) {
                    return $model->fechaEspana($model->fechaIncicio);
                }
            ],
            //'fechaFinal',
            [
                'attribute' => 'fechaIncicio',
                'value' => function ($model) {
                    return $model->fechaEspana($model->fechaIncicio);
                }
            ],
            'lugaresAVisitar:ntext',
            'gastronomia:ntext',
            'historia:ntext',
            //'continente',
            [
                'attribute' => 'continente',
                'value' => function ($model) {
                    return $model->continente0->continente;
                }
            ],

        ],
    ]) ?>

</div>