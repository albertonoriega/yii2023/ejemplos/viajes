<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Lugardestino $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="lugardestino-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idLugar')->input('number') ?>

    <?= $form->field($model, 'lugar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pais')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaIncicio')->input('date') ?>

    <?= $form->field($model, 'fechaFinal')->input('date') ?>

    <?= $form->field($model, 'lugaresAVisitar')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gastronomia')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'historia')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'archivo')->fileInput() ?>

    <?= Html::img('@web/imgs/' . $model->foto, ['class' => 'col-lg-2']) ?>

    <?= $form->field($model, 'continente')->dropDownList($model->continentes, ['prompt' => 'Selecciona continente']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>