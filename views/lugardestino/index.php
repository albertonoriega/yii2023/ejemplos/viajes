<?php

use app\models\Lugardestino;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Lugar de destinos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lugardestino-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="far fa-plus-square" style="font-size:1.5em;position:relative;top:2px"></i> Nuevo lugar de destino', ['create'], ['class' => 'btn btn-success', 'style' => 'font-weight:bold']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idLugar',
            //'foto',
            'lugar',
            'pais',
            //'fechaIncicio',
            //'fechaFinal',
            //'lugaresAVisitar:ntext',
            //'gastronomia:ntext',
            //'historia:ntext',
            //'continente',
            [
                'attribute' => 'continente',
                'value' => function ($model) {
                    if (isset($model->continente0)) {
                        return $model->continente0->continente;
                    }
                }
            ],
            [
                'label' => 'Foto',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->foto)) {
                        return Html::img("@web/imgs/{$dato->foto}", ["width" => 200, "height" => 200]);
                    } else {
                        return Html::img("@web/imgs/notfound.jpg", ["width" => 200, "height" => 200]);
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Lugardestino $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idLugar' => $model->idLugar]);
                }
            ],
        ],
    ]); ?>


</div>