<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Lugardestino $model */

$this->title = 'Actualizar lugar de destino: ' . $model->idLugar;
$this->params['breadcrumbs'][] = ['label' => 'Lugardestinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idLugar, 'url' => ['view', 'idLugar' => $model->idLugar]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="lugardestino-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formUpdate', [
        'model' => $model,
    ]) ?>

</div>