<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Lugardestino $model */

$this->title = 'Nuevo lugar de destino';
$this->params['breadcrumbs'][] = ['label' => 'Lugar de destinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lugardestino-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formInsert', [
        'model' => $model,
    ]) ?>

</div>