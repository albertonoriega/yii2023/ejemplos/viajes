<?php

use yii\bootstrap5\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Viajes', 'url' => ['/site/continentes']],
        ['label' => ' Login ', 'url' => ['/site/login'], 'linkOptions' => ['class' => "fas fa-user", 'style' => 'position:relative;top:5px']],
    ]
]);
