<?php

use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Viajes', 'url' => ['/site/continentes']],
        ['label' => 'Administración', 'items' => [
            ['label' => 'Destino', 'url' => ['lugardestino/index/']],
            ['label' => 'Continente', 'url' => ['/continente/index']],
        ]],

        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Cerrar sesión (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'

    ]
]);
