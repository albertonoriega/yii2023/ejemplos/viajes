<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Continente $model */

$this->title = 'Crear continente';
$this->params['breadcrumbs'][] = ['label' => 'Continentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="continente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>