<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Continente $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="continente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->input('number') ?>

    <?= $form->field($model, 'continente')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>