<?php

use app\models\Continente;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Continentes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="continente-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="far fa-plus-square" style="font-size:1.5em;position:relative;top:2px"></i> Nuevo continente', ['create'], ['class' => 'btn btn-success', 'style' => 'font-weight:bold']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'continente',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Continente $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>