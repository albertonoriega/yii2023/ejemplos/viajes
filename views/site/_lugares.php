<?php

use yii\helpers\Html;

?>

<div class="lugar">

    <h3><?= $dato['lugar'] ?></h3>


    <p><?= $dato['pais'] ?></p>

    <p>
        <?php
        if (isset($dato['foto'])) {
            echo Html::img("@web/imgs/{$dato['foto']}", ['style' => 'width:200px;height: 200px', 'class' => 'img-thumbnail']);
        } else {
            echo  Html::img("@web/imgs/notfound.jpg", ['style' => 'width:200px;height: 200px', 'class' => 'img-thumbnail']);
        }
        ?>
    </p>

    <?= Html::a('Más detalles', ['site/verlugar', 'idLugar' => $dato->idLugar], ['class' => 'botonDetalles']) ?>

</div>