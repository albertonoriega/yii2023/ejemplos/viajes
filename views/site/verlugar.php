<?php

use yii\helpers\Html;
?>

<h2 style="text-align: center;margin:25px"> <?= $dato->lugar ?></h2>
<table class="tablaLugar">
    <tr>
        <td>Lugar</td>
        <td><?= $dato->lugar ?></td>
    </tr>
    <tr>
        <td>País</td>
        <td><?= $dato->pais ?></td>
    </tr>
    <tr>
        <td>Foto</td>
        <td><?= Html::img("@web/imgs/{$dato->foto}", ['width' => '600px', 'class' => 'img-fluid']) ?></td>
    </tr>

    <tr>
        <td>Fecha de inicio</td>
        <td><?= $dato->fechaEspana($dato->fechaIncicio) ?></td>
    </tr>
    <tr>
        <td>Fecha final</td>
        <td><?= $dato->fechaEspana($dato->fechaFinal) ?></td>
    </tr>
    <tr>
        <td>Lugares a visitar</td>
        <td> <?= $dato->lugaresAVisitar ?></td>
    </tr>
    <tr>
        <td>Gastronomía</td>
        <td> <?= $dato->gastronomia ?></td>
    </tr>
    <tr>
        <td>Historia</td>
        <td> <?= $dato->historia ?></td>
    </tr>
    <tr>
        <td>Continente</td>
        <td> <?= $dato->continente0->continente ?></td>
    </tr>

</table>