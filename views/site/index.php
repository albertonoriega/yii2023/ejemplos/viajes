<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4" style="color:purple;">Viaja por placer, no para escapar de la rutina</h1>
    </div>

    <div class="body-content">
        <div style="text-align: center;">
            <?php
            echo Html::img('@web/imgs/mapa.png', ['class' => 'img-fluid'])
            ?>
        </div>
    </div>
</div>