<?php

use yii\helpers\Html;

?>
<div class="row">
    <?php
    foreach ($continentes as $continente) {
    ?>
        <div class="col-4 mb-3">
            <div class="card cartas">
                <div class="card-body">
                    <h5 class="card-title"><?= $continente->continente ?></h5>
                    <?= Html::a('Ver lugares', ['site/lugares', 'id' => $continente->id], ['class' => 'btn btn-light']) ?>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>